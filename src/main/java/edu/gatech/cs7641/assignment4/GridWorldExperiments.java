package edu.gatech.cs7641.assignment4;

import burlap.behavior.policy.EpsilonGreedy;
import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.PolicyUtils;
import burlap.behavior.singleagent.Episode;
import burlap.behavior.singleagent.auxiliary.EpisodeSequenceVisualizer;
import burlap.behavior.singleagent.auxiliary.StateReachability;
import burlap.behavior.singleagent.auxiliary.valuefunctionvis.ValueFunctionVisualizerGUI;
import burlap.behavior.singleagent.learning.LearningAgent;
import burlap.behavior.singleagent.learning.tdmethods.QLearning;
import burlap.behavior.singleagent.planning.Planner;
import burlap.behavior.singleagent.planning.stochastic.policyiteration.PolicyIteration;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.valuefunction.QProvider;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.singleagent.gridworld.GridWorldTerminalFunction;
import burlap.domain.singleagent.gridworld.GridWorldVisualizer;
import burlap.domain.singleagent.gridworld.state.GridAgent;
import burlap.domain.singleagent.gridworld.state.GridLocation;
import burlap.domain.singleagent.gridworld.state.GridWorldState;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.environment.SimulatedEnvironment;
import burlap.mdp.singleagent.oo.OOSADomain;
import burlap.statehashing.HashableStateFactory;
import burlap.statehashing.simple.SimpleHashableStateFactory;
import burlap.visualizer.Visualizer;
import com.github.sh0nk.matplotlib4j.Plot;
import com.github.sh0nk.matplotlib4j.PythonExecutionException;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author James MacGlashan.
 * @useForClass Jeff Miller
 */
public class GridWorldExperiments {

    private GridWorldDomain gwdg;
    private OOSADomain domain;
    private State initialState;
    private HashableStateFactory hashingFactory;
    private SimulatedEnvironment env;

    private int maxX;
    private int maxY;
    private String outputPath;

    private interface StopWatchFunction {
        void timeit();
    }

    private void timeit(final StopWatchFunction stopWatchFunction) {
        Instant start = Instant.now();
        stopWatchFunction.timeit();
        Instant end = Instant.now();
        System.out.println("Duration: " + Duration.between(start, end));
    }

    public void init4x4(final String outputPath) {
        this.outputPath = outputPath;
        this.maxX = 4;
        this.maxY = 4;

        this.gwdg = new GridWorldDomain(this.maxX, this.maxY);
        this.gwdg.setProbSucceedTransitionDynamics(0.9);
        this.gwdg.setMap(new int[][]{
                {0, 0, 0, 0},
                {0, 0, 1, 0},
                {0, 1, 0, 0},
                {0, 0, 0, 0}
        });

        TerminalFunction tf = new GridWorldTerminalFunction(3, 3);
        this.gwdg.setTf(tf);

        this.domain = gwdg.generateDomain();

        this.initialState = new GridWorldState(new GridAgent(0, 0), new GridLocation(3, 3, "terminal"));
        this.hashingFactory = new SimpleHashableStateFactory();

        this.env = new SimulatedEnvironment(this.domain, this.initialState);

//        VisualActionObserver observer = new VisualActionObserver(this.domain, GridWorldVisualizer.getVisualizer(this.gwdg.getMap()));
//        observer.initGUI();
//        this.env.addObservers(observer);
    }

    public void init16x16(final String outputPath) {
        this.outputPath = outputPath;
        this.maxX = 16;
        this.maxY = 16;

        this.gwdg = new GridWorldDomain(this.maxX, this.maxY);
        this.gwdg.setProbSucceedTransitionDynamics(0.9);
        this.gwdg.setMap(new int[][]{
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1},
                {0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0},
                {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0},
                {1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1},
                {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
                {0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
                {1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
        });

        TerminalFunction tf = new GridWorldTerminalFunction(14, 14);
        this.gwdg.setTf(tf);

        this.domain = this.gwdg.generateDomain();

        this.initialState = new GridWorldState(new GridAgent(0, 0), new GridLocation(14, 14, "terminal"));
        this.hashingFactory = new SimpleHashableStateFactory();

        this.env = new SimulatedEnvironment(this.domain, this.initialState);

//        VisualActionObserver observer = new VisualActionObserver(this.domain, GridWorldVisualizer.getVisualizer(this.gwdg.getMap()));
//        observer.initGUI();
//        this.env.addObservers(observer);
    }

    public void visualize() {
        Visualizer v = GridWorldVisualizer.getVisualizer(this.gwdg.getMap());
        new EpisodeSequenceVisualizer(v, this.domain, this.outputPath);
    }

    public void policyIterationExample() {
//        final List<Double> gammas = Arrays.asList(0.9, 0.99);
        final List<Double> gammas = Arrays.asList(0.9);

        for (final Double gamma : gammas) {
            System.out.println("Policy Iteration (gamma): " + gamma);
            timeit(() -> {
                Planner planner = new PolicyIteration(this.domain, gamma, this.hashingFactory, 0.001, 100, 100);
                Policy p = planner.planFromState(this.initialState);

                Episode episode = PolicyUtils.rollout(p, this.initialState, this.domain.getModel());
                episode.write(this.outputPath + "pi");
                System.out.println("Policy Iteration Max Time Step: " + episode.maxTimeStep());

                simpleValueFunctionVis((ValueFunction) planner, p);
            });
            System.out.println("");
        }
    }

    public void valueIterationExample() {
//        final List<Double> gammas = Arrays.asList(0.9, 0.99);
        final List<Double> gammas = Arrays.asList(0.9);

        for (final Double gamma : gammas) {
            System.out.println("Value Iteration (gamma): " + gamma);
            timeit(() -> {
                Planner planner = new ValueIteration(this.domain, gamma, this.hashingFactory, 0.001, 100);
                Policy p = planner.planFromState(this.initialState);

                Episode episode = PolicyUtils.rollout(p, this.initialState, this.domain.getModel());
                episode.write(this.outputPath + "vi");
                System.out.println("Value Iteration Max Time Step: " + episode.maxTimeStep());

                simpleValueFunctionVis((ValueFunction) planner, p);
            });
            System.out.println("");
        }
    }


    public void qLearningExample() {
//        final List<Double> gammas = Arrays.asList(0.9, 0.99);
        final List<Double> gammas = Arrays.asList(0.99);
//        final List<Double> qInits = Arrays.asList(0.0, 0.5, 1.);
        final List<Double> qInits = Arrays.asList(1.);
//        final List<Double> learningRates = Arrays.asList(0.7, 0.6);
        final List<Double> learningRates = Arrays.asList(.6);
        final List<Double> epsilons = Arrays.asList(0.1, 0.2, .3, .4, .5);
//        final List<Double> epsilons = Arrays.asList(.1);

        final Plot plt = Plot.create();

        plt.xlabel("Iteration");
        plt.ylabel("Steps");
        plt.title("QLearner Epsilon");
        plt.legend();

        for (final Double gamma : gammas) {
            for (final Double qInit : qInits) {
                for (final Double learningRate : learningRates) {
                    for (final Double epsilon : epsilons) {

                        System.out.println("QLearning (gamma): " + gamma);
                        System.out.println("QLearning (qInit): " + qInit);
                        System.out.println("QLearning (learningRate): " + learningRate);
                        System.out.println("QLearning (epsilon): " + epsilon);

                        timeit(() -> {
                            LearningAgent agent = new QLearning(this.domain, gamma, this.hashingFactory, qInit, learningRate);
                            ((QLearning) agent).setLearningPolicy(new EpsilonGreedy((QProvider) agent, epsilon));

                            final List<Integer> iterations = new ArrayList<>();
                            final List<Integer> steps = new ArrayList<>();

                            //run learning for 50 episodes
                            for (int i = 0; i < 200; i++) {
                                Episode episode = agent.runLearningEpisode(this.env);

                                if (i >= 195) {
                                    episode.write(this.outputPath + "ql_" + i);
                                    System.out.println("QLearning_" + i + " Max Time Step: " + episode.maxTimeStep());
                                }

                                iterations.add(i + 1);
                                steps.add(episode.maxTimeStep());

                                //reset environment for next learning episode
                                this.env.resetEnvironment();
                            }

                            plt.plot()
                                    .add(iterations, steps)
                                    .label("Epsilon: " + epsilon)
                                    .linestyle("--");

                            final Integer sum = steps.stream().reduce((a, b) -> a + b).get();
                            System.out.println("Average steps: " + ((double) sum / steps.size()));

                            simpleValueFunctionVis((ValueFunction) agent, new GreedyQPolicy((QProvider) agent));
                        });
                        System.out.println("");
                    }
                }
            }
        }


        // plt.show();

        final String fname = this.outputPath + "qlearner.png";
        plt.savefig(fname).dpi(200);
        // Don't miss this line to output the file!
        try {
            plt.executeSilently();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (PythonExecutionException e) {
            e.printStackTrace();
        }
    }

    public void simpleValueFunctionVis(ValueFunction valueFunction, Policy p) {
        double value = 0.0;
        double count = 0;

        List<State> allStates = StateReachability.getReachableStates(this.initialState, this.domain, this.hashingFactory);

        for (final State state : allStates) {
            value += valueFunction.value(state);
            count++;
        }

        System.out.println("Average Reward: " + (value / count));

        ValueFunctionVisualizerGUI gui = GridWorldDomain.getGridWorldValueFunctionVisualization(allStates, this.maxX, this.maxY, valueFunction, p);
        gui.initGUI();
    }

    public static void main(String[] args) {
        GridWorldExperiments example = new GridWorldExperiments();

//        example.init4x4("output/04/");
        example.init16x16("output/16/");
        example.valueIterationExample();
        example.policyIterationExample();
        example.qLearningExample();
        example.visualize();

    }

}
